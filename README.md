## missi-user 12 SP1A.210812.016 21.11.8 release-keys
- Manufacturer: xiaomi
- Platform: mt6893
- Codename: ares
- Brand: redmi
- Flavor: missi-user
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: 21.11.8
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: zh-CN
- Screen Density: 440
- Fingerprint: redmi/vnd_ares/ares:12/SP1A.210812.016/21.11.8:user/release-keys
- OTA version: 
- Branch: missi-user-12-SP1A.210812.016-21.11.8-release-keys
- Repo: redmi_ares_dump_3351


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
