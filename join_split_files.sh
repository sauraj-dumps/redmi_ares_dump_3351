#!/bin/bash

cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat vendor/lib64/librelight_only.so.* 2>/dev/null >> vendor/lib64/librelight_only.so
rm -f vendor/lib64/librelight_only.so.* 2>/dev/null
cat system/system/lib64/librelight_only_extraphoto.so.* 2>/dev/null >> system/system/lib64/librelight_only_extraphoto.so
rm -f system/system/lib64/librelight_only_extraphoto.so.* 2>/dev/null
cat system/system/data-app/MIUIYoupin/MIUIYoupin.apk.* 2>/dev/null >> system/system/data-app/MIUIYoupin/MIUIYoupin.apk
rm -f system/system/data-app/MIUIYoupin/MIUIYoupin.apk.* 2>/dev/null
cat system/system/data-app/com.autonavi.minimap_12/com.autonavi.minimap_12.apk.* 2>/dev/null >> system/system/data-app/com.autonavi.minimap_12/com.autonavi.minimap_12.apk
rm -f system/system/data-app/com.autonavi.minimap_12/com.autonavi.minimap_12.apk.* 2>/dev/null
cat system/system/data-app/MIShop/MIShop.apk.* 2>/dev/null >> system/system/data-app/MIShop/MIShop.apk
rm -f system/system/data-app/MIShop/MIShop.apk.* 2>/dev/null
cat system/system/data-app/com.baidu.searchbox_9/com.baidu.searchbox_9.apk.* 2>/dev/null >> system/system/data-app/com.baidu.searchbox_9/com.baidu.searchbox_9.apk
rm -f system/system/data-app/com.baidu.searchbox_9/com.baidu.searchbox_9.apk.* 2>/dev/null
cat system/system/data-app/com.taobao.taobao_24/com.taobao.taobao_24.apk.* 2>/dev/null >> system/system/data-app/com.taobao.taobao_24/com.taobao.taobao_24.apk
rm -f system/system/data-app/com.taobao.taobao_24/com.taobao.taobao_24.apk.* 2>/dev/null
cat system/system/data-app/MIUIVipAccount/MIUIVipAccount.apk.* 2>/dev/null >> system/system/data-app/MIUIVipAccount/MIUIVipAccount.apk
rm -f system/system/data-app/MIUIVipAccount/MIUIVipAccount.apk.* 2>/dev/null
cat system/system/data-app/SmartHome/SmartHome.apk.* 2>/dev/null >> system/system/data-app/SmartHome/SmartHome.apk
rm -f system/system/data-app/SmartHome/SmartHome.apk.* 2>/dev/null
cat system/system/data-app/com.UCMobile_11/com.UCMobile_11.apk.* 2>/dev/null >> system/system/data-app/com.UCMobile_11/com.UCMobile_11.apk
rm -f system/system/data-app/com.UCMobile_11/com.UCMobile_11.apk.* 2>/dev/null
cat system/system/data-app/MIFinance/MIFinance.apk.* 2>/dev/null >> system/system/data-app/MIFinance/MIFinance.apk
rm -f system/system/data-app/MIFinance/MIFinance.apk.* 2>/dev/null
cat system/system/data-app/MIUIEmail/MIUIEmail.apk.* 2>/dev/null >> system/system/data-app/MIUIEmail/MIUIEmail.apk
rm -f system/system/data-app/MIUIEmail/MIUIEmail.apk.* 2>/dev/null
cat system/system/priv-app/MIUIMusic/MIUIMusic.apk.* 2>/dev/null >> system/system/priv-app/MIUIMusic/MIUIMusic.apk
rm -f system/system/priv-app/MIUIMusic/MIUIMusic.apk.* 2>/dev/null
cat system/system/priv-app/MIUIBrowser/MIUIBrowser.apk.* 2>/dev/null >> system/system/priv-app/MIUIBrowser/MIUIBrowser.apk
rm -f system/system/priv-app/MIUIBrowser/MIUIBrowser.apk.* 2>/dev/null
cat system/system/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null >> system/system/priv-app/MiuiCamera/MiuiCamera.apk
rm -f system/system/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null
cat system/system/priv-app/MIUISecurityCenter/MIUISecurityCenter.apk.* 2>/dev/null >> system/system/priv-app/MIUISecurityCenter/MIUISecurityCenter.apk
rm -f system/system/priv-app/MIUISecurityCenter/MIUISecurityCenter.apk.* 2>/dev/null
cat system/system/priv-app/MIUIGallery/MIUIGallery.apk.* 2>/dev/null >> system/system/priv-app/MIUIGallery/MIUIGallery.apk
rm -f system/system/priv-app/MIUIGallery/MIUIGallery.apk.* 2>/dev/null
cat system/system/priv-app/MIUIVideos1/MIUIVideos1.apk.* 2>/dev/null >> system/system/priv-app/MIUIVideos1/MIUIVideos1.apk
rm -f system/system/priv-app/MIUIVideos1/MIUIVideos1.apk.* 2>/dev/null
cat system/system/app/MiGameService_MTK/MiGameService_MTK.apk.* 2>/dev/null >> system/system/app/MiGameService_MTK/MiGameService_MTK.apk
rm -f system/system/app/MiGameService_MTK/MiGameService_MTK.apk.* 2>/dev/null
cat system/system/app/VoiceAssist/VoiceAssist.apk.* 2>/dev/null >> system/system/app/VoiceAssist/VoiceAssist.apk
rm -f system/system/app/VoiceAssist/VoiceAssist.apk.* 2>/dev/null
cat bootimg/01_dtbdump_MT6893.dtb.* 2>/dev/null >> bootimg/01_dtbdump_MT6893.dtb
rm -f bootimg/01_dtbdump_MT6893.dtb.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
